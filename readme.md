# CSE482 Assignment
3/24/2016 12:37:21 AM  

### Live Search with XHR

- implement it with non-persistent array 
- with persistent mysql DB

<!--<img src="./demo.gif">-->
![demo gif](demo.gif?raw=true)

<br><br>
<hr>

<br>  



## Notes:

- native XHR requests are much faster. [proof!](https://jsperf.com/native-xhr-vs-jquery-ajax/27)  
- to avoid cache you can append random numbers at the end of query. e.g. `search/?rand=539fs2304`  
- handle exception or else you'll rot in hell  
- use `mysql_error()` in case of debugging